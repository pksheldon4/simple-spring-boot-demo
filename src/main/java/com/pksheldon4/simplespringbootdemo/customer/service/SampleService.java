package com.pksheldon4.simplespringbootdemo.customer.service;

import com.pksheldon4.simplespringbootdemo.model.CustomConfigModel;
import com.pksheldon4.simplespringbootdemo.exceptions.SomethingBadHappenedException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class SampleService {

    @Autowired
    private CustomConfigModel customConfigModel;


    public String getSampleMessage(Integer value) {

        if (customConfigModel != null) {
            log.info(customConfigModel.getCustomNestedConfiguration().getNestedStringValue());
        }

        if (value % 2 == 0) {
            throw new SomethingBadHappenedException(value);
        } else {
            return "Valid Odd Number";
        }
    }

}
