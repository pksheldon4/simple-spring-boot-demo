package com.pksheldon4.simplespringbootdemo.customer.endpoint;

import com.pksheldon4.simplespringbootdemo.model.Payload;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@Slf4j
@Validated
public class ValidatingController {

    private static final String SUCCESSFUL_VALIDATION = "Successful Validation";

    @PostMapping("/validated")
    public ResponseEntity<String> validatedPayload(@Valid @RequestBody Payload payload) {

        log.info(payload.toString());
        return ResponseEntity.ok("Successful Validation");
    }

    @PostMapping("/validatePayload")
    public String validatedPayload(@Valid @RequestBody Payload payload, HttpServletRequest request) {

        log.info(payload.toString());
        log.info(request.getContentType());
        return SUCCESSFUL_VALIDATION;
    }
}
