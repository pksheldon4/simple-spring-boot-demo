package com.pksheldon4.simplespringbootdemo.customer.endpoint;

import com.pksheldon4.simplespringbootdemo.customer.service.SampleService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SampleController {

    private final SampleService sampleService;

    public SampleController(SampleService service) {
        this.sampleService = service;
    }

    @GetMapping("/sample")
    public String sample() {
        return sampleService.getSampleMessage(1);
    }
}
