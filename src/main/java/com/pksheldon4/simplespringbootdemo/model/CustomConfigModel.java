package com.pksheldon4.simplespringbootdemo.model;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "custom.config")
public class CustomConfigModel {

    private String someString;
    private boolean someFlag;
    private CustomConfigNestedModel customNestedConfiguration;
}
