package com.pksheldon4.simplespringbootdemo.model;

import lombok.Data;

@Data
public class CustomConfigNestedModel {

    private String nestedStringValue;
    private Integer nestedIntegerValue;
}
