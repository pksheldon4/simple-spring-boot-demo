package com.pksheldon4.simplespringbootdemo.model;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class Payload {

    @NotNull(message = "First Name cannot be null")
    private String firstName;
    @NotNull(message = "Last Name cannot be null")
    private String lastName;
    private String title;

    public Payload() {

    }
}
