package com.pksheldon4.simplespringbootdemo.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class SampleControllerAdvice {

    @ExceptionHandler(value = {RuntimeException.class})
    protected ResponseEntity defaultHandle() {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }

    @ExceptionHandler(value = {SomethingBadHappenedException.class})
    protected ResponseEntity handleEvenNumberException() {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }
}
