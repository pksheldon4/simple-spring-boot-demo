package com.pksheldon4.simplespringbootdemo.exceptions;

public class SomethingBadHappenedException extends RuntimeException {
    public SomethingBadHappenedException(int value) {
        super("Invalid Even Number - " + value);
    }
}
