package com.pksheldon4.simplespringbootdemo;

import com.pksheldon4.simplespringbootdemo.model.CustomConfigModel;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties(CustomConfigModel.class)
public class SimpleSpringBootDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SimpleSpringBootDemoApplication.class, args);
    }

}
