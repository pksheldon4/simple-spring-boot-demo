package com.pksheldon4.simplespringbootdemo.customer.service;

import com.pksheldon4.simplespringbootdemo.exceptions.SomethingBadHappenedException;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

class SampleServiceTest {

    SampleService sampleService = new SampleService();

    @Test
    public void sampleService_throwsEvenNumberException_whenPassedAnEvenNumbeR() {

        SomethingBadHappenedException exception = assertThrows(SomethingBadHappenedException.class, () -> {
            sampleService.getSampleMessage(2);
        });
        assertThat(exception.getMessage()).isEqualTo("Invalid Even Number - 2");
    }

}