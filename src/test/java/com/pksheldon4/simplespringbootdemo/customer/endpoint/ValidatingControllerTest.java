package com.pksheldon4.simplespringbootdemo.customer.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pksheldon4.simplespringbootdemo.model.Payload;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class ValidatingControllerTest {

    private MockMvc mockMvc;
    private ObjectMapper objectMapper = new ObjectMapper();

    @BeforeEach
    public void setUp() {

        mockMvc = MockMvcBuilders.standaloneSetup(ValidatingController.class)
            .build();
    }


    @Test
    public void sampleController_validPayload_returnsSuccessful() throws Exception {

        Payload payload = new Payload();
        payload.setFirstName("SomeOne");
        payload.setLastName("Special");
        payload.setTitle("Developer");

        mockMvc.perform(post("/validated")
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsBytes(payload)))
            .andExpect(status().isOk());
    }

    @Test
    public void sampleController_incompletePayyload_returnsBadRequest() throws Exception {
        mockMvc.perform(post("/validated")
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsBytes(new Payload())))
            .andExpect(status().isBadRequest());
    }
}