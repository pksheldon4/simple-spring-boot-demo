package com.pksheldon4.simplespringbootdemo.customer.endpoint;

import com.pksheldon4.simplespringbootdemo.exceptions.SampleControllerAdvice;
import com.pksheldon4.simplespringbootdemo.exceptions.SomethingBadHappenedException;
import com.pksheldon4.simplespringbootdemo.customer.service.SampleService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class SampleControllerTest {

    private MockMvc mockMvc;
    private SampleService service;

    @BeforeEach
    public void setUp() {
        service = mock(SampleService.class);
        SampleController controller = new SampleController(service);
        mockMvc = MockMvcBuilders.standaloneSetup(controller)
            .setControllerAdvice(SampleControllerAdvice.class)
            .build();
    }

    @Test
    public void sampleController_successfullyReturns() throws Exception {

        when(service.getSampleMessage(1)).thenReturn("Test Message");

        MvcResult result = mockMvc.perform(get("/sample"))
            .andExpect(status().isOk())
            .andExpect(content().string("Test Message"))
            .andReturn();

        assertThat(result).isNotNull();
        assertThat(result.getResponse().getContentAsString()).isNotEmpty();
    }


    @Test
    public void sampleController_returnsBadRequest_whenServiceThrowsSomethingBadHappenedException() throws Exception {

        when(service.getSampleMessage(any())).thenThrow(SomethingBadHappenedException.class);

        mockMvc.perform(get("/sample"))
            .andExpect(status().isBadRequest());
    }


    @Test
    public void sampleController_returnsInternalServerError_whenServiceThrowsRuntimeException() throws Exception {

        when(service.getSampleMessage(any())).thenThrow(RuntimeException.class);

        mockMvc.perform(get("/sample"))
            .andExpect(status().isInternalServerError());
    }

}